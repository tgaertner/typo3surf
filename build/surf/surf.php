<?php
$workflow = new \TYPO3\Surf\Domain\Model\SimpleWorkflow();

$node = new \TYPO3\Surf\Domain\Model\Node('Testing');

//$node->setHostname('p447309.webspaceconfig.de');
//$node->setOption('username', 'p447309');

$node->setHostname('192.168.182.16');                             // <- fill in
$node->setOption('username', 'vagrant');    // <- fill in

// avoid cleaning of cache directory
$node->setOption('hardClean', FALSE);


$application = new \TYPO3\Surf\Application\TYPO3\CMS();
$application->setName('typo3surf');
//$application->setDeploymentPath('/html/htdocs');               // <- fill in
$application->setDeploymentPath('/var/www');               // <- fill in
$application->setOption('repositoryUrl', 'git@bitbucket.org:jocmoc/typo3surf.git');
$application->setOption('keepReleases', 2);
$application->addNode($node);
$application->setOption('composerCommandPath', '/usr/local/bin/composer');

$workflow->setEnableRollback(TRUE);

// *** set user permissions
$workflow->defineTask('benaja.typo3surf:setPermission',
    \TYPO3\Surf\Task\ShellTask::class,
    array('command' => 'cd {releasePath}/web && sudo chown -R www-data:www-data *')
);

// *** simple smoke test
$smokeTestOptions = array(
    'url' => 'http://next.typo3surf.local',
    'remote' => TRUE,
    'expectedStatus' => 200,
    'expectedRegexp' => '/HELLO Surf/'
);
$workflow->defineTask('benaja.typo3surf:smokeTest', \TYPO3\Surf\Task\Test\HttpTestTask::class, $smokeTestOptions);


$deployment->onInitialize(function() use ($workflow, $application) {
    $workflow->addTask('benaja.typo3surf:setPermission', 'migrate', $application);
//    $workflow->addTask('benaja.typo3surf:smokeTest', 'test', $application);
});

$deployment->setWorkflow($workflow);
$deployment->addApplication($application);
?>